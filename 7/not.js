
// Напишите функцию, которая считает, сколько раз определённый символ встречается в строке.

function tF(str) {
  var result = {};
  var a = str.split("");
  for (var i = 0; i < a.length; i++) {
    var b = result[a[i]]? result[a[i]] : 0;
    result[a[i]] = b + 1;
  }
  return result;
}

function countChars_short(str) {
  return str.split("").reduce((r, c) => (r[c] = (r[c] || 0) + 1, r), {});
}

console.log(tF("корабли лавировали, лавировали да не выловировали"));




// Напишите функцию, которая выводит в консоль текущие дату, месяц и год в формате «24 января 2019».

function tS() {
    Data = new Date();
    Year = Data.getFullYear();
    Month = Data.getMonth();
    Day = Data.getDate();


    switch (Month) {
        case 0:
            fMonth = "января";
            break;
        case 1:
            fMonth = "февраля";
            break;
        case 2:
            fMonth = "марта";
            break;
        case 3:
            fMonth = "апреля";
            break;
        case 4:
            fMonth = "мая";
            break;
        case 5:
            fMonth = "июня";
            break;
        case 6:
            fMonth = "июля";
            break;
        case 7:
            fMonth = "августа";
            break;
        case 8:
            fMonth = "сентября";
            break;
        case 9:
            fMonth = "октября";
            break;
        case 10:
            fMonth = "ноября";
            break;
        case 11:
            fMonth = "декабря";
            break;
    }


    document.write(Day + " " + fMonth + " " + Year + " года");
}

tS()


// Напишите функцию, которая определяет, является ли строка палиндромом.
// Учитывайте, что пробелы и знаки препинания не влияют на «палиндромность» строки!


const isPalindrome = (str) => {
  const a = /[^A-Za-z0-9-А-Я]/g;
  const cleanedStr = str.replace(a, '').toLowerCase();
  // строка в массив
  const splitArray = cleanedStr.split('');
  // переворот букв
  const reverseArray = splitArray.reverse();
  // новая строка из перевернутого массива
  const reverseString = reverseArray.join('');

  if( cleanedStr === reverseString) {
    return true;
  } else {
    return false;
  }
}

console.log(isPalindrome('table'));
console.log(isPalindrome('Анна'));




// Напишите функцию, которая заменяет все повторяющиеся буквы в строке на звёздочки.  РАЗОБРАТЬСЯ почему рядом стоящие заменяются а не все
// Например, строка "я учусь программированию" должна преобразоваться в "я уч*сь прог*ам*и**в*н*ю".
/*

clearString = str => {
    const a = str.match(/(.)\1{1,}/g)
    for(let b in a) {
        str = str.replace(a[b], a[b].substr(0,1)+'*')
    }
    return str;
}
console.log(clearString('я учусь программированию aabc11d111110000маамафффалла'))
*/




// Напишите функцию, которая замяняет первую букву каждого слова в строке на такую же большую.

let str = "корабли лавировали, лавировали да не выловировали";

function tT(str) {

    return str.replace(/(^|\s)\S/g,
        function(a) {return a.toUpperCase()})

}

document.writeln(tT(str));


//6 Напишите функцию, которая возвращает текущий день недели на русском языке.

function tS() {
    var days = [
        'Воскресенье',
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота'
    ];
    var d = new Date();
    var n = d.getDay();
    console.log(days[n]);
}
tS()

/*Напишите функцию, которая принимает у пользователя дату в формате "ДД-ММ-ГГГГ" и,
используя функцию из задачи 6, выдаёт в консоль день недели для этой даты*/


function tSs() {
    var days = [
        'Воскресенье',
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота'
    ];

    var d = document.getElementById('date_input').valueAsDate;
    var n = d.getDay()
    document.getElementById("output").innerHTML = days[n];
}




// Примите у пользователя день его рождения в формате "ДД-ММ-ГГГГ". Напишите функцию,
// которая выводит в консоль количество дней, оставшихся до его дня рождения.

// Напишите другую функцию, которая возвращает дату, в которую пользователь отметит
// ближайший 1000-дневный юбилей (например, когда ему исполнится 12000 дней).


function daysLeft() {

    var year = parseInt(prompt('введите год (format: YYYY)'));
    var month = parseInt(prompt('введите месяц (format: M)'));
    var day = parseInt(prompt('введите день'));

    var today = new Date();
    today.setHours(0,0,0,0);
    var nextDate = new Date([today.getFullYear(),month,day].join(','));
    if (nextDate < today) nextDate.setFullYear(today.getFullYear()+1);

    mDay = 24*60*60*1000;
    daysLeft = Math.round((nextDate.getTime() - today.getTime())/mDay);
    dayname = "";
    a = ""+daysLeft;

    b=parseInt(a.substr(a.length-1));

    if(daysLeft>4&&daysLeft<21)dayname=" дней";
    else
    if(b==1)dayname=" день";
    else
    if(b==2||b==3||b==4)dayname=" дня";
    else dayname=" дней";

    if(daysLeft==0) {alert("День рождения!");}
    else {if(daysLeft==1) {alert("Завтра праздник!");}
    else {alert("до ДР осталось"+" " + daysLeft+dayname+"!");}
    }
}
daysLeft()


