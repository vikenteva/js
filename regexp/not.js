/* Создайте селект с несколькими опциями. Добавьте одну опцию, используя Javascript.
Сделайте так, чтобы по выбору опции выводилось сообщение с данными этой опции (текст + значение).*/

function fun1() {
    var sel=document.getElementById('mySelect').selectedIndex;
    var options=document.getElementById('mySelect').options;
    alert(''+options[sel].text+' '+options[sel].value);
}



//Создайте регулярное выражение для поиска трёх точек.

let regexp = /\.{3,}/g;
console.log( "Привет!... Как дела?... Как жизнь?...".match(regexp) );



//Создайте regexp, который ищет все положительные числа, в том числе десятичные.

let b = /[+-]?\d*\.?\d{1,30}/gi;
let st = "0781234567, .1564529874654654231, 684984654657465, 078454321, 3264556, 0.25642"
console.log(st.match(b));



//Создайте регулярку, которая ищет цвета в формате #eee, #eeeddd

let a = /#[a-f0-9]{6}\b/i;
let str = "color:#FFFFFF, #12345678; background-color:#AA00ef bad-colors:f#fddee #fd2"
console.log(str.match(a));



//Предложите строку, которая подойдет под выражение ^$
console.log('Пустая строка ""');



/*Используя регулярные выражения, напишите скрипт, который заменяет слово «функция» в тексте на «function».
Если получится, сделайте так, чтобы «функция» заменялась в любом падеже.*/

stri = "функция"
let q = stri.replace(/функция/i,"function");
console.log(q);



//С помощью регулярных выражений определите, является ли строка корректным временем вида '09.59 am', '12.30 pm'.

let regex = /^([0]\d|[1][0-2]):([0-5]\d)\s?(?:AM|PM)$/i;
let timeStr = ["09.59 am", "12.30 pm","11:59AM","09:69PM", "09:24pm", "09:99pm", "15:23bm", "09:23 AM", "14:74 PM"];
for (i=0; i<timeStr.length; i++)
    console.log("Время: "+timeStr[i]+ " " +regex.test(timeStr[i]));



// Создайте форму вычисления процентов по вкладу:

function getChar(event) {

    if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which)
    }

    return null; // специальная клавиша
}

var form = document.forms.calculator;


var moneyElem = form.elements.money;
moneyElem.onkeypress = function(e) {
    e = e || event;
    var chr = getChar(e);

    if (e.ctrlKey || e.altKey || chr == null) return; // специальная клавиша
    if (chr < '0' || chr > '9') return false;
}

moneyElem.onkeyup = calculate;   // клавиатура, вставить/вырезать клавиатурой

moneyElem.oninput = calculate;


var capitalizationElem = form.elements.capitalization;
capitalizationElem.onclick = calculate;

var monthsElem = form.elements.months;
monthsElem.onchange = calculate;

function calculate() {
    var sum = +moneyElem.value;
    if (!sum) return;

    var monthlyIncrease = 0.01;

    if (!capitalizationElem.checked) {
        sum = sum * (1 + monthlyIncrease * monthsElem.value);
    } else {

        for (var i = 0; i < monthsElem.value; i++) {
            // 1000 1010 1020.1
            sum = sum * (1 + monthlyIncrease);
        }
    }
    sum = Math.round(sum);

    var height = sum / moneyElem.value * 100 + 'px';
    document.getElementById('height-after').style.height = height;
    document.getElementById('money-before').innerHTML = moneyElem.value;
    document.getElementById('money-after').innerHTML = sum;
}

calculate();

