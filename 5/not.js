
//Создайте объект obj = {a: 1, b: 2, c: 3}. Выведите в консоль элемент с ключом 'c' двумя способами:
// через квадратные скобки и через точку. Затем выведите все свойства объекта через цикл for..in.

let obj = {a: 1, b: 2, c: 3};
console.log(obj['c']);
console.log(obj.c);

for(let d in obj)
	console.log(d+obj[d]);



//Создайте объект city, добавьте ему свойства name (название города, строка) и population (население, число).
function task (){
let city= {
	name: "minsk",
	population: 1949070,
};
}

// 3 Создайте массив из шести объектов такого же вида, как city из задачи 2 – по одному для каждого областного города Беларуси.

var city = [
    {  name: "minsk",
		population: 1949070},

    { name: "gomel",
		population: 526901},

    { name: "vitebsk",
	population: 370298},

    { name: "brest",
	population: 337445 },

    { name: "mogilew",
		population: 374713 },

    { name: "grodno",
		population: 361445 },
];


//Напишите функцию, которая принимает массив из задачи 3 в качестве
// параметра и выводит в консоль информацию о каждом городе.

function taskOne (a) {
    for (let b in city)
        console.log(b, city [b]);
}

taskOne(city)


//Создайте в объектах с городами из задачи 3 метод getInfo, который возвращает строку  с информацией о городе
// (например, в таком формате: "Город Добруш, население – 18760 человек").

function taskTwo(a) {

  a.getInfo = function () {
        result = a[4];
        console.log("Город" + " " + result.name + " " + "население -" + " " + result.population +" " + "человек");
    };
    a.getInfo();
}

taskTwo(city)



//Создайте несколько (3-4) объектов одинаковой структуры, которые описывают
// ваши любимые книги (автор, название, год издания + любые другие свойства на ваше усмотрение).
// Добавьте в каждый из них метод для вывода в консоль краткой информации о книге (например, в формате Автор — Название).


class Book{
 constructor (name, autor, data) {
     this.name = name;
     this.autor = autor;
     this.data = data;

 }
}

var a = new Book("Мастер и Маргарита", "Булгаков Михаил Афанасьевич", 1984);
var b = new Book("7 навыков высокоэффективнных людей", "Стивен Кови", 1989);
var c = new Book("Дом Странных Детей", "Ренсом Риггз", 2012);

console.log("Автор" + " " + a.autor + " " + "Название" + " " + a.name);
console.log("Автор" + " " + b.autor + " " + "Название" + " " + b.name);
console.log("Автор" + " " + c.autor + " " + "Название" + " " + c.name);



//Создайте функцию, которая получает в качестве аргументов два объекта с книгами  (из задачи 3) и возвращает ту книгу, которая издана раньше.

function taskFour ( m, n) {
if (a.data>b.data)
{
    return b.data;
}
else
    return a.data;
}
console.log(taskFour(a[0], b[1]));




//Создайте объект с информацией о себе (имя, фамилия, любимое занятие).
// Добавьте в него метод, который выводит эту информацию в консоль в удобочитаемом формате.

function taskThree(){
let pepson ={
	name: "Elena",
	surname	: "Vikenteva",
	activity: "snowboarding"
}
	console.log("Меня зовут" +" "+ pepson.name +" "+ "фамилия" +" "+ pepson.surname +" "+ "любимое занятие" +" "+ pepson.activity );
}
taskThree()


//Создайте объект "Цилиндр" (свойства – радиус и высота). Добавьте в него метод,
// который считает объём цилиндра (используя this).

function taskFour(){
    let pi = 3.14;
    let obj ={
        radius: 20,
    height: 30,
};
    obj.volume = function  (){
    let= res = pi* this.height * this.radius *this.radius
        console.log(res);
    }
    obj.volume();
}
console.log(taskFour())


//Выберите пингвина из списка вымышленных пингвинов на Википедии и опишите
// его в виде объекта (не менее трёх полей; например, имя, создатель и источник).
// Добавьте этому объекту свойство canFly. Добавьте два метода: sayHello, который выводит
// в консоль приветствие и представление вашего пингвина, и fly, который в зависимости от значения свойства
// canFly (true или false) определяет, может ли пингвин летать, и сообщает об этом в консоль.

function taskFive() {
	let penguin = {
		name: "Капитан Кук",
		author: "Пингвины мистера Поппера",
		source: "Ричард и Флоренс Этуотер",
		canFly: true
	};
    function penguinSayHello () {
		console.log("Привет, меня зовут" + " " + penguin.name);
	}
	function penguinFly () {
		if (this.canFly == true) {
			console.log("Летаю");
		} else {
			console.log("Не умею летать")
		}
	}

	penguinSayHello();
	penguinFly();
}
console.log(taskFive())








//Создайте функцию, которая принимает два числа, и определяет,
// делится ли одно из них на другое без остатка. Из функции верните true или false.

function tTwo () {

    let a = prompt('Введите первое число');
    let b = prompt('Введите второе число');

    res = a % b == 0 ? 'true' : 'false';

    document.write(res)
}
tTwo()



// Создайте функцию, которая получает в качестве аргумента оценку по 10-балльной шкале.
// Для ошибочных аргументов (0, «B») функция возвращает текстовую ошибку, для верных – их текстовое описание из Википедии.

function four() {
    let numFirst = prompt('Введите число от 1 до 10');
    switch (numFirst) {
        case 0 :
            console.log("ошибка");
            break;

        case '1' :
        case  '2' :
            console.log("Неудовлетворительно");
            break;

        case '3':
        case '4':
            console.log("Удовлетворительно");
            break;

        case '5':
            console.log("Почти хорошо");
            break;

        case '6' :
            console.log("Хорошо");
            break;

        case '7':
            console.log("Очень хорошо");
            break;

        case '8':
            console.log("Почти отлично");
            break;

        case '9':
            console.log("Отлично");
            break;

        case '10':
            console.log("Блестяще");
            break;

        default:
            console.log("Ошибка");
    }
}

four()



//Создайте функцию, которая принимает в качестве аргумента целое число, соответствующее порядковому номеру месяца.
// Если месяц с таким номером существует, функция возвращает имя сезона (лето, осень, зима, весна),
// к которому относится месяц. Если нет – сообщение об ошибке.

function five() {
    let month = prompt('Введите число от 1 до 12');
    switch (month) {
        case '1' :
        case  '2' :
        case  '12' :
            console.log("Зима");
            break;

        case '3':
        case '4':
        case '5':
            console.log("Весна");
            break;

        case '6':
        case '7':
        case '8':
            console.log("Лето");
            break;

        case '9' :
        case '10' :
        case '11' :
            console.log("Осень");
            break;

        default:
            console.log("Такого месяца нет");
    }
}

five()


//  Используя цикл do..while, создайте функцию, которая принимает числа через prompt(), пока не будет введено число 0.

function six_one () {
    let num = 0, y;

    function six(x) {
        let a;
        while (Number((a = prompt('Введите число:')) != x))
            alert('Введи другое число!');
        alert('Ура!');
        return Number(a);
    }

    y = six(num);
    console.log(y);
}
six_one()



// Создайте функцию, которая выводит в консоль числа от 10 до 99, заканчивающиеся на 7 или делящиеся на 7 (в убывающем порядке).

function seven(a) {
    for (i = 10; i <= 100; i++) {
        if (i % 7 == 1 || i % 7 >= 1) {
            continue;
        }
 document.getElementById('new').innerHTML += i + ' ';
    }}
seven(a)



// Создайте функцию, которая принимает два аргумента – количество учеников в классе и количество парт в кабинете.
// Функция возвращает строку вида «не хватает парт: 2» / «лишних парт: 1». Важно: за одной партой может сидеть два ученика!

function eight () {

    let pupilsTable = 2;
    let pupils = parseInt(prompt('Введите количество учеников'));
    let tables = parseInt(prompt('Введите количество парт'));

    if (tables * pupilsTable >= pupils) {
        alert(Math.floor(tables - pupils / pupilsPerTable) + ' лишних парт');
    } else {
        alert(Math.ceil(pupils / pupilsTable - tables) + ' парты не хватает');
    }
}

eight()




//Создайте функцию, которая получает два числа и возвращает знак их произведения. Если результат равен нулю, возвращать ‘+’.

function tTen () {

    let a = prompt('Введите первое число');
    let b = prompt('Введите второе число');

    res = a * b == 0 ? '+': '';
    document.write(res)
}
tTen()




// Создайте функцию, которая получает число и выводит в консоль все его делители.

function eleven() {
    let num = parseInt(prompt('Введите число'));
    var arr = [];     // сюда идут делители
    if (num < 0) {
        num = num * -1;
    }
    for (let i = 1; i <= num; i++) {

        if (num % i == 0) {
            arr.push(i);
            arr.push(i * -1);
        }
    }
console.log(arr)}
eleven()



// Создайте функцию words(),  которая в зависимости от переданного в нее целого числа n, будет
// выводить слово «карандаш» с правильным окончанием («12 карандашей», но «22 карандаша»).

function words (num, a) {
    if (num > 100) num = num % 100;
    if (num <= 20 && num >= 10) return a[2];
    if (num > 20) num = num % 10;
    return num === 1 ? a[0] : num > 1 && num < 5 ? a[1] : a[2];
}

document.getElementById("form").onsubmit = function () {
    let count = +document.getElementById("input").value;
    document.getElementById("users").innerHTML = count + " " + words(count, ["карандаш", "карандаша", "карандашей"]);
    return false
}

