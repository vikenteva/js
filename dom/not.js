//Все элементы label внутри таблицы. Должно быть 3 элемента.

let elements = document.getElementsByTagName('label')
for (let elem of elements) {
    alert(elem.innerHTML);}



//Первую ячейку таблицы (со словом "Возраст").

var table = document.getElementById('age-table');
alert( table.rows[0].cells[0].innerHTML);



//Вторую форму в документе.

let elements = document.getElementsByTagName('form')[1];
console.log(elements);



//Форму с именем search, без использования её позиции в документе.

let form = document.forms.search;
console.log(form);



// Элемент input в форме с именем search. Если их несколько, то нужен первый.

let elements = document.getElementsByTagName('form')[0];
let search = document.querySelectorAll('input')[0];
console.log(elements);
console.log(search);



//Элемент с именем info[0], без точного знания его позиции в документе. ???

let elements = document.getElementsByTagName('input');
let search = document.getElementsByClassName('info')[0];
console.log(elements);
console.log(search);



//Элемент с именем info[0], внутри формы с именем search-person. ???

let elements = document.getElementsByTagName('form')[1];
let search = document.querySelectorAll('info')[0];

console.log(elements);
console.log(search);



//Дан элемент #elem. Добавьте ему класс "www".

let elem = document.getElementById('elem');
elem.classList.add('www');



//Дан элемент #elem. Проверьте наличие у него класса "www", если есть — удалите этот класс.

let ele = document.getElementById('elem'); // проверка
alert(ele.classList.contains('www'));
alert(ele.classList.remove('www')); // удаляем



//Дан ul. Дан массив. Вставьте элементы этого массива в конец ul так, чтобы каждый элемент стоял в своем li. Сделайте так, чтобы четные позиции имели красный фон.

let ul = document.getElementById('ul');
let arr = [1, 2, 3, 4, 5, 6];

for(let i = 0; i < arr.length; i++) {
   // if ((arr[i] % 2) === 0) {

        let li = document.createElement('li');

        li.innerHTML = arr[i];
        ul.appendChild(li);
    }



//Дан элемент #elem. Найдите его соседа сверху и добавьте ему в начало и в конец по восклицательному знаку '!'

let elemm = document.getElementById('elemm');
let prev = elemm.previousElementSibling;

prev.insertAdjacentHTML('afterBegin', ' ! ')
prev.insertAdjacentHTML('beforeEnd', '! ')


